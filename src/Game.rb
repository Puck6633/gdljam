class Game < Chingu::Window
  attr_reader :heading, :description;
  attr_accessor :score;

  def instance()
    self.new() unless @@instance;
    return @@instance;
  end

  def initialize()
    @@instance = self;

    opts = Trollop::options {
      opt :width, "Screen width in pixels", :short => 'w', :type => :int, :default => 1024;
      opt :height, "Screen height in pixels", :short => 'h', :type => :int, :default => 768;
      opt :resolution, "Screen resolution in pixels (e.g. 1024x768)", :short => 'r', :type => :string;
      opt :fullscreen, "Whether to use fullscreen mode or not", :short => 'f', :default => false;
    }

    if (opts[:resolution])
      matched = /(\d+)[\/x\*](\d+)/.match(opts[:resolution]);
      opts[:width],opts[:height] = [$1.to_i, $2.to_i] if matched;
    end

    super(opts[:width], opts[:height], opts[:fullscreen]);
    self.caption = "GDL Like a Boss Jam";

    @heading = Gosu::Font.new($window, "media/OpenSans-SemiBold.ttf", 18);
    @description = Gosu::Font.new($window, "media/OpenSans-Regular.ttf", 15);
    @score = 0;

    push_game_state(MainMenu);
  end
end
