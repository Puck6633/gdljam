#Solve a bug in Gosu where text always defaults to bold and add a helper function
#for drawing wrapping text
class Gosu::Font
  def draw_text(text, x, y, z = 0, scale_x = 1, scale_y = 1, color = 0xff_ffffff, mode = :default)
    self.draw("</b>#{text}", x, y, z, scale_x, scale_y, color, mode);
  end

  def draw_text_wrap(text, x, y, width, z = 0, scale_x = 1, scale_y = 1, color = 0xff_ffffff, mode = :default, spacing = 4)
    line = String.new();
    cur_y = y;
    text.split(" ").each { |word|
      if (self.text_width(line+word+" ") < width)
        line += " " if line.length
        line += word;
      else
        self.draw_text(line, x, cur_y, z, scale_x, scale_y, color, mode);
        line = String.new();
        cur_y += self.height+spacing;
      end
    }
    self.draw_text(line, x, cur_y, z, scale_x, scale_y, color, mode);
  end
end

#Enable scaling of the entire playfield
# class Chingu::GameObject
#   def draw
#     @image.draw_rot(@x*$window.factor, @y*$window.factor, @zorder, @angle, @center_x, @center_y, @factor_x*$window.factor, @factor_y*$window.factor, @color, @mode)  if @image and $window
#   end
#
#   def draw_relative(x=0, y=0, zorder=0, angle=0, center_x=0, center_y=0, factor_x=0, factor_y=0)
#     @image.draw_rot((@x+x)*$window.factor, (@y+y)*$window.factor, @zorder+zorder, @angle+angle, @center_x+center_x, @center_y+center_y, (@factor_x+factor_x)*$window.factor, (@factor_y+factor_y)*$window.factor, @color, @mode)  if @image and $window
#   end
#
#   def draw_at(x, y)
#     draw_relative(x, y)
#   end
# end
