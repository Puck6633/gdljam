class Command
  attr_accessor :type, :param;

  def initialize(type, param)
    @type = type;
    @param = param;
  end
end

class Drone < GameObject
  traits :sprite, :velocity, :bounding_box, :collision_detection;

  @@turnrate = 4.5;
  @@acceleration = 0.4;
  @@drag = 0.95;

  attr_accessor :selected;

  def initialize(options = {})
    super(options.merge!(:image => "ship.png"));
    @script = options[:script];
    @selected = false;
    @hp = 100;
    @commands = Array.new;
  end

  def action_turnleft();  @angle -= @@turnrate; end
  def action_turnright(); @angle += @@turnrate; end
  def action_thrust()
    self.velocity_x += offset_x(@angle, @@acceleration);
    self.velocity_y += offset_y(@angle, @@acceleration);
  end

  def set_command(command)
    @commands = Array.new();
    add_command(command);
  end

  def add_command(command)
    @commands.push(command) if command.is_a? Command;
  end

  def update()
    super();

    if @script.is_a?(Node)
      @script.exec(self);
    elsif @script.is_a?(Array)
      @script.each {|node| n.exec(self)};
    end

    self.velocity_x *= @@drag;
    self.velocity_y *= @@drag;
    @x = @x % @parent.viewport.game_area.right;
    @y = @y % @parent.viewport.game_area.bottom;

    case (@commands[0].type)
      when :move
        target = @commands[0].param;
        angle = Math.atan2(@commands[0].param[1]-@y, @commands[0].param[0]-@x);
      when :shoot
    end

    @hp -= rand.round();
    @parent.remove_game_object(self) if (@hp <= 0);
  end

  def draw()
    super();
    $window.draw_circle(@x, @y, self.width*0.6*$window.factor, Color.argb(0xff_28a228)) if @selected;
    $window.fill_rect([@x-self.width/2, @y-self.height/2-8, self.width, 6], Color.argb(0xaf_333333));
    $window.fill_rect([@x-self.width/2, @y-self.height/2-8, self.width*[0, @hp].max/100, 6], Color.argb(0xff_bd3333));
  end
end
