class VisualNode < GameObject
  trait :bounding_box;
  attr_accessor :selected;

  def initialize(options = {})
    super(options);
    @type = options[:type] || "UNKNOWN";
    @next = nil;
    @center_x = @center_y = 0;
    @selected = false;
    @protected = options[:protect] || false;
  end

  def size
    [266, 114];
  end

  def click()
    @selected = true;
  end

  def drag(dx, dy)
    @x = (@x+dx).clamp(@parent.viewport.game_area.left, @parent.viewport.game_area.right-self.bb.width);
    @y = (@y+dy).clamp(@parent.viewport.game_area.top, @parent.viewport.game_area.bottom-self.bb.height);
  end

  def draw()
    $window.fill_rect([@x+8, @y, 258, 114], Color.argb(0xff_8a8a12));
    $window.fill_rect([@x+8, @y, 250, 114], Color.argb(0xff_001200));
    $window.fill_rect([@x+8, @y, 250, 24], Color.argb(0xff_0d200d));
    $window.draw_rect([@x+8, @y, 250, 114], @selected ? Color.argb(0xff_8a8a12) : Color.argb(0xff_528052));
    $window.heading.draw_text(@type, @x+12, @y+4);
    $window.description.draw_text_wrap("This event occurs each time the AI is ready to process a new command.", @x+12, @y+28, 250);
    super();
  end
end
