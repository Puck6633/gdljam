class NewNodeMenu < GameObject
  traits :bounding_box;

  def initialize()
    super();
  end

  def draw()
    $window.heading.draw_text("This should be <c=ffff00>yellow</c>", 10, 10, 1.0, 1.0, 1.0);
    $window.description.draw_text("This should be <b>bold</b>", 10, 30, 1.0, 1.0, 1.0);
    $window.description.draw_text("This should be <i>italic</i>", 10, 50, 1.0, 1.0, 1.0);
    $window.description.draw_text("This should be <u>underlined</u>", 10, 70, 1.0, 1.0, 1.0);
  end
end
