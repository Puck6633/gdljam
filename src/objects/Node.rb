class Node
  @@types = [
    :event_wake,
    :event_sleep,
    :action_turnleft,
    :action_turnright,
    :action_thrust,
    :action_fire
  ];

  attr_reader :next;

  def initialize(type)
    @type = type;
  end

  def next=(otherNode)
    @next = otherNode if otherNode.is_a? Node;
  end

  def exec(target)
    target.send(@type) if target.respond_to?(@type);
    @next.exec(target) if @next;
  end
end
