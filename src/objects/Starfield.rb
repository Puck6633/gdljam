class Starfield < GameObject
  traits :sprite;

  def initialize(options = {})
    super(options.merge!(:image => "space.png", :zorder => -100));
  end

  def draw()
    startx = @parent.viewport.x%self.image.width;
    starty = @parent.viewport.y%self.image.height;
    (-starty..starty+$window.height).step(self.image.height*$window.factor) { |y|
      (-startx..startx+$window.width).step(self.image.width*$window.factor) { |x|
        self.image.draw(@parent.viewport.x+x, @parent.viewport.y+y, @zorder, @factor_x*$window.factor, @factor_y*$window.factor);
      }
    }
  end
end
