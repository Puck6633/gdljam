class MainMenu < GameState
  def setup()
    @mainmenu = Chingu::SimpleMenu.create({
      :menu_items => {
        "Start" => Play,
        "Quit" => :close_game
      },
    }) unless @mainmenu;
    self.input = {
      :escape => :close_game
    }

    @music = Song['Music4.ogg'] unless @music;
    @music.play(true);

    $window.cursor = false;
  end

  def finalize()
    @music.stop();
  end
end
