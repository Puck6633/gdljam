class Play < PannableGameState
  def initialize(options = {})
    super();
    self.viewport.game_area = [0, 0, 10000, 10000];
    self.viewport.x_target = 5000-$window.width*$window.factor;
    self.viewport.y_target = 5000-$window.height*$window.factor;
    @selectbox = Rect.new([0, 0, 0, 0]);
    @selecting = false;
    @zoom = 0.5;

    root = Node.new(:event_wake);
    root.next = Node.new(:action_thrust);
    #root.next.next = Node.new(:action_turnright);
    (4000..6000).step(50) { |y|
      (4000..6000).step(50) { |x|
        Drone.create(:x => x, :y => y);
      }
    }
    @background = Image['space.png'];

    self.input = {
      :escape => :close,
      :wheel_up => lambda { @zoom *= 1.25 },
      :wheel_down => lambda { @zoom *= 0.75 },
      :mouse_left => :click,
      :holding_mouse_left => :drag,
      :released_mouse_left => :drop
    }

    @music = Song['Music1.wav'] unless @music;
    @music.play(true);

    $window.cursor = true;
  end

  def click()
    @selectbox.topleft = [$window.mouse_x, $window.mouse_y];
    @selecting = true;
  end

  def drag()
    @selectbox.size = [$window.mouse_x-@selectbox.left, $window.mouse_y-@selectbox.top];
  end

  def drop()
    @selecting = false;
    scalefactor = 1/@zoom;
    # @selectbox.topleft = [self.viewport.x+@selectbox.left*2, self.viewport.y+@selectbox.left*2];
    @selectbox.topleft = [self.viewport.x+@selectbox.left*scalefactor, self.viewport.y+@selectbox.top*scalefactor];
    @selectbox.size = [@selectbox.width*scalefactor, @selectbox.height*scalefactor];
    Drone.each { |d|
      d.selected = (d.bounding_box.collide_rect?(@selectbox));
    }
  end

  def zoom(level)
    diff = 1+(level-@zoom);
    @zoom = level;
    self.viewport.x_target *= diff;
    self.viewport.y_target *= diff;
  end

  def draw()
    startx = -(self.viewport.x*@zoom % @background.width);
    starty = -(self.viewport.y*@zoom % @background.height);
    (starty..starty+$window.height+@background.width).step(@background.height*@zoom) { |y|
      (startx..startx+$window.width+@background.height).step(@background.width*@zoom) { |x|
        @background.draw(x, y, -1000, @zoom, @zoom);
      }
    }

    $window.scale(@zoom) { super(); }
    $window.draw_rect(@selectbox, Color.argb(0xff_28a228), 1000) if @selecting;
  end

  def finalize()
    @music.stop();
    $window.factor = 1;
  end
end
