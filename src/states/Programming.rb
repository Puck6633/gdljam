class Programming < PannableGameState
  def setup()
    @addmenu = NewNodeMenu.create() unless @addmenu;
    @test = VisualNode.create(:x => 100, :y => 100, :type => "Brain::Wake");

    @addmenu.hide!;
    self.viewport.game_area = [0, 0, 10000, 10000];

    self.input = {
      :return => Play,
      :escape => :close,
      :space => :show_menu,
      :mouse_left => :click,
      :holding_mouse_left => :drag
    }

    @music = Song['Music3.wav'] unless @music;
    @music.play(true);

    $window.cursor = true;
  end

  def finalize()
    @music.stop();
  end

  def show_menu
    @addmenu.show!;
  end

  def click()
    @prevx, @prevy = $window.mouse_x, $window.mouse_y;
    if (@addmenu.visible? and @addmenu.bb.collide_point?($window.mouse_x, $window.mouse_y)) #Clicked on add menu?
      @addmenu.send(:click);
    else
      intersected = VisualNode.all.reject{|node| not node.bb.collide_point?($window.mouse_x, $window.mouse_y) }; #Find nodes under mouse
      VisualNode.each {|node| node.selected = false } if intersected.length < 1; #Deselect when not clicking anything
      intersected.each { |node| node.send(:click) }; #Send click event to nodes under mouse
    end
  end

  def drag()
    dx, dy = $window.mouse_x-@prevx, $window.mouse_y-@prevy;
    VisualNode.each { |node|
      node.send(:drag, dx, dy) if node.selected;
    }
    @prevx, @prevy = $window.mouse_x, $window.mouse_y;
  end

  def draw()
    #Draw background grid
    fill(Color.argb(0xff_0d200d));
    grid_size = 20;
    grid_spacing = 1;
    (-(self.viewport.y%grid_size)..$window.width).step(grid_size+grid_spacing) { |y|
      (-(self.viewport.x%grid_size)..$window.width).step(grid_size+grid_spacing) { |x|
        fill_rect([x, y, grid_size, grid_size], Color.argb(0xff_001200));
      }
    }

    super(); #Draw GameObjects
  end

  def update()
    #@prevx, @prevy = $window.mouse_x, $window.mouse_y;
    super();
  end
end
