class PannableGameState < GameState
  trait :viewport;

  def initialize()
    super();
    @scrollspeed = 10;
  end

  def input=(input_list)
    super(input_list.merge!({
      [:space, :mouse_right] => :start_pan,
      [:holding_space, :holding_mouse_right] => :drag_pan,
      :holding_up => lambda {self.viewport.y_target -= @scrollspeed},
      :holding_down => lambda {self.viewport.y_target += @scrollspeed},
      :holding_left => lambda {self.viewport.x_target -= @scrollspeed},
      :holding_right => lambda {self.viewport.x_target += @scrollspeed}
    }));
  end

  def start_pan()
    @mousex, @mousey = [$window.mouse_x, $window.mouse_y];
    @startx, @starty = [self.viewport.x_target, self.viewport.y_target];
  end

  def drag_pan()
    self.viewport.x_target = @startx-($window.mouse_x-@mousex);
    self.viewport.y_target = @starty-($window.mouse_y-@mousey);
  end

  def update()
    super();
    # self.viewport.x_target = self.viewport.x_target.clamp(self.viewport.game_area.left, self.viewport.game_area.right-$window.width*$window.factor);
    # self.viewport.y_target = self.viewport.y_target.clamp(self.viewport.game_area.top, self.viewport.game_area.bottom-$window.height*$window.factor);
  end
end
